import os
import sys
import re
from collections import Counter
from collections import defaultdict
if len(sys.argv) != 3:
    print("Correct format to run is: svm_testing_formater.py path_to_directory format_file")
    sys.exit(1)
path = sys.argv[1]
fil1 = open(sys.argv[2],"w")
n = 0
c = Counter()
d = defaultdict()
word = 1
d1 = 1
c1 = 0
c2 = -1
with open("svm_dictionary", encoding='utf-8', errors='ignore') as fil2:
        for line in fil2:
            www=line.partition(' ')[0]
            line=line.split(' ', 1)[1]
            d[www] = int(line)
with open("svm_refer","r") as myfil:
    for line in myfil:
        word = line.partition(' ')[0]
        count = line.split(' ',1)[1]
        c[word] = int(count)
        c1 = c1 + 1
myfil.close()
if c1 > 2:
    print("Your training data has more than two classes")
else:
    for dir_entry in sorted(os.listdir(path)):
        #print(dir_entry)
        dir_entry_path = os.path.join(path, dir_entry)
        d2 = defaultdict()
        d3 = defaultdict()
        if os.path.isfile(dir_entry_path):
            with open(dir_entry_path, encoding='utf-8', errors='ignore') as my_file:
                inp = my_file.read()
                inp = inp.replace("\n"," ")
                temp = dir_entry
                clas = temp[0:temp.find(".")]
                words=re.findall(r'[^\s!,.?":;)(*&^%$#@]+', inp)
                #words = inp.split()
                for j in words:
                    if j in d2:
                        d2[j]+=1
                    else:
                        d2[j]=1
                #print(d2)
                my_file.close()
                if c[clas] == -1:
                    fil1.write("-1")
                else:
                    fil1.write("+1")
                for j in words:
                    if j in d:
                        d3[d[j]] = j
                for j in sorted(d3.keys()):
                    ttt = d3[j]
                  #  print(ttt)
                    fil1.write(" "+str(d[ttt])+":"+str(int(d2[ttt])))
                    
                fil1.write("\n")     
#print(word)
fil1.close()
