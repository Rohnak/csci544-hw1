import sys
from collections import Counter
c = Counter()
if len(sys.argv) != 3:
    print("Correct format to run is: svm_process.py predictions_file processed_file")
    sys.exit(1)
path = sys.argv[1]
with open("svm_refer","r") as fil1:
    for line in fil1:
        word = line.partition(' ')[0]
        count = line.split(' ',1)[1]
        c[int(count)] = word
f = open(sys.argv[2],"w")
with open(path,"r") as fil:
    for line in fil:
        key = line.partition('\t')[0]
        if float(key) <= 0:
            key = -1
        else:
            key = 1
        f.write(c[int(key)]+"\n")
f.close()
fil1.close()
