import os
import re
import sys
from collections import Counter
from collections import defaultdict
if len(sys.argv) != 3:
    print("Correct format to run is: mega_format_test.py path_to_directory format_file")
    sys.exit(1)
path = sys.argv[1]
c = Counter()
c2 = 1
fil = open(sys.argv[2],"w")
fil.write("TEST"+"\n")
with open("mega_refer","r") as fil1:
    for line in fil1:
        word = line.partition(' ')[0]
        count = line.split(' ',1)[1]
        c[word] = int(count)
fil1.close()
for dir_entry in sorted(os.listdir(path)):
    dir_entry_path = os.path.join(path, dir_entry)
    d = defaultdict()
    if os.path.isfile(dir_entry_path):
        with open(dir_entry_path, encoding='utf-8', errors='ignore') as my_file:
            inp = my_file.read()
            inp = inp.replace("\n"," ")
            temp = dir_entry
            clas = temp[0:temp.find(".")]
            words=re.findall(r'[^\s!,.?":;0-9]+', inp)
            for j in words:
                if j != '#':
                    if j in d:
                        d[j]+=1
                    else:
                        d[j]=1
            fil.write(str(c[clas]))
            for j in d:
                fil.write(" "+j+" "+str(d[j]))
            fil.write("\n")
fil.close()
