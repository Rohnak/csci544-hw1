import sys
from collections import Counter
c = Counter()
path = sys.argv[1]
with open("mega_refer","r") as fil1:
    for line in fil1:
        word = line.partition(' ')[0]
        count = line.split(' ',1)[1]
        c[int(count)] = word
f = open(sys.argv[2],"w")
with open(path,"r") as fil:
    for line in fil:
        key = line.partition('\t')[0]
        f.write(c[int(key)]+"\n")
f.close()
fil1.close()
