import os
import re
import sys
from collections import Counter
from collections import defaultdict
if len(sys.argv) != 3:
    print("Correct format to run is: megam_format.py path_to_directory format_file")
    sys.exit(1)
path = sys.argv[1]
c = Counter()
c2 = 0
fil = open(sys.argv[2],"w")
for dir_entry in os.listdir(path):
    temp = dir_entry
    clas = temp[0:temp.find(".")]
    if clas not in c:
        c[clas]=c2
        c2 = c2 + 1
fil1 = open("mega_refer","w")
for i in c:
    fil1.write(i+" "+str(c[i])+"\n")
fil1.close()
for dir_entry in os.listdir(path):
    dir_entry_path = os.path.join(path, dir_entry)
    d = defaultdict()
    if os.path.isfile(dir_entry_path):
        with open(dir_entry_path, encoding='utf-8', errors='ignore') as my_file:
            inp = my_file.read()
            inp = inp.replace("\n"," ")
            temp = dir_entry
            clas = temp[0:temp.find(".")]
            words=re.findall(r'[^\s!,.?":;0-9]+', inp)
            for j in words:
                if j!= '#':
                    if j in d:
                        d[j]+=1
                    else:
                        d[j]=1
            fil.write(str(c[clas]))
            for j in d:
                fil.write(" "+j+" "+str(d[j]))
            fil.write("\n")
fil.close()
