Following files are for part 1:

preprocess_train.py - For preprocessing the training data into format required by nblearn.
to run: python3 preprocess_train.py directory_containing_training_data formated_file

preprocess_test.py - For preprocessing the testing data into format required by nbclassify.
to run: python3 preprocess_test.py directory_containing_testing_data formated_file

nblearn - same as specification

nbclassify - same as specification

spam.nb - model file for spam data

sentiment.nb - model file for sentiment data

spam.out - output for spam data

sentiment.out - output for sentiment data
