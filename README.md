Precision, Recall and Fscores for Part1:

1. FOR SPAM:
     Precision = 353/367 = 0.962
     Recall = 353/363 = 0.972
     F-score = 0.967
2. FOR HAM:
     Precision = 986/996 = 0.989
     Recall = 986/1000 = 0.986
     F-score = 0.9879
3. FOR POS (When 30% of data is used as development and 70% as training)
     Precision = 3037/3480 = 0.873
     Recall = 3037/3752 = 0.809
     F-score = 0.839
4. FOR NEG (When 30% of data is used as development and 70% as training)
     Precision = 3305/4020 = 0.822
     Recall = 3305/3740 = 0.884
     F-score = 0.8509
 
Precision, Recall and Fscores for Part2:

SVM-
1. FOR SPAM:
     Precision = 301/307 = 0.9804
     Recall = 301/363 = 0.8292
     F-score = 0.8985
2. FOR HAM:
     Precision = 994/1056 = 0.9412
     Recall = 994/1000 = 0.994
     F-score = 0.9669
3. FOR POS (When 30% of data is used as development and 70% as training)
     Precision = 3315/3875 = 0.8554
     Recall = 3315/3752 = 0.8835
     F-score = 0.869
4. FOR NEG (When 30% of data is used as development and 70% as training)
     Precision = 3188/3625 = 0.879
     Recall = 3188/3748 = 0.8505
     F-score = 0.864

MEGAM- (Using Binary classes and not multiclass)
1. FOR SPAM:
     Precision = 357/368 = 0.9701
     Recall = 357/363 = 0.9834
     F-score = 0.9767
2. FOR HAM:
     Precision = 989/995 = 0.9939
     Recall = 989/1000 = 0.989
     F-score = 0.9914
3. FOR POS (When 30% of data is used as development and 70% as training)
     Precision = 3178/3752 = 0.8470
     Recall = 3178/3585 = 0.8864
     F-score = 0.866
4. FOR NEG (When 30% of data is used as development and 70% as training)
     Precision = 3341/3915 = 0.8533
     Recall = 3341/3748 = 0.8914
     F-score = 0.871

When only 10% the data is used:

For spam detection:(of 1846 training documents approximately 1300 were HAM and the rest were spam)
The Fscore, Recall and Precision in case of naive baye's for spam detection remain almost the same i.e they fall nearly about 2-3%.
SVM for fscore, recall and precision dropped considerably for the SPAM class in spam detection.
For Megam it predicted everything as spam.
Reason: High bias/ Low variance classifier like naive bayes does not overfit incase of small datasets, while megam and svm seem to overfit.

For sentiment analysis: (of 2500 training documents approximately 1230 were POS and the rest NEG)
The fscore, recall and precision for naive baye's and SVM dropped only by 5-7 %, whereas it dropped considerably(by 10%) for megam.
Reason: Megam performance drops as the training set becomes small.
