import sys
import re
from math import log
from collections import defaultdict
if len(sys.argv) != 3:
    print("Correct format to run is: nbclassify.py model_file test_file")
    sys.exit(1)
f = open(sys.argv[1],"r")
n = int(f.readline())
clas = []
clas_n = []
clas_w = []
d = defaultdict(list)
n1 = 0
n2 = 0
dt = open("dictionary","w")
for i in range(0,n):
    line = f.readline()
    clas_entry=line.partition(' ')[0]
    clas.append(clas_entry)
    line=line.split(' ', 1)[1]
    clas_n.append(int(line))   
for i in range(1,n+1):
    n2 = 0
    line = f.readline()
    clas_entry=line.partition(' ')[0]
    line=line.split(' ', 1)[1]
    for j in range(0,int(line)):
        line2 = f.readline()
        word = line2.partition(' ')[0]
        count = line2.split(' ',1)[1]
        n2 = n2 + int(count)
        if len(d[word]) < i-1:
            for lll in range(len(d[word])+1,i):
                d[word].append(0);
        d[word].append(int(count))
    clas_w.append(n2)
for kk in d:
    if len(d[kk]) < n:
        for lll in range(len(d[kk])+1,n+1):
            d[kk].append(0)
for kk in d:
    n1+=1
    dt.write(kk+" ")
    dt.write(str(d[kk]))
    dt.write("\n")
f.close()
no_docs = 0
for i in clas_n:
    no_docs = no_docs + i
argmax = []
a = 0
b = 0
c = 0
with open(sys.argv[2], encoding='utf-8', errors='ignore') as f:
    for line in f:
        words=re.findall(r'[^\s!,.?":;0-9]+', line)
        argmax = []
        for i in range(0,len(clas)):
            a = 0            
            a = a + log(clas_n[i]) - log(no_docs)
            for j in words:
                if j in d:
                    lst = d[j]
                    a = a + log(lst[i]+1) - log(clas_w[i]+n1)
                else:
                    a = a + log(1) - log(clas_w[i]+n1)
            argmax.append(a)
        t = argmax.index(max(argmax))
        print(clas[t])
