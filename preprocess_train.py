import os
import sys
if len(sys.argv) != 3:
    print("Correct format to run is: preprocess_train.py path_to_directory format_file")
    sys.exit(1)
path = sys.argv[1]
f = open(sys.argv[2],"w")
n = 0
for dir_entry in os.listdir(path):
    dir_entry_path = os.path.join(path, dir_entry)
    if os.path.isfile(dir_entry_path):
        with open(dir_entry_path, encoding='utf-8', errors='ignore') as my_file:
            inp = my_file.read()
            inp = inp.replace("\n"," ")
            temp = dir_entry
            f.write(temp[0:temp.find(".")]+" "+inp)
            f.write("\n")
            n = n+1
            #print(n)
            my_file.close()
f.close()
