import os
import sys
import collections
import re
if len(sys.argv) != 3:
    print("Correct format to run is: nblearn.py training_file model_file")
    sys.exit(1)
path = sys.argv[1]
c = collections.Counter()
n1 = 0
lf = open(sys.argv[2],"w")
with open(path, encoding='utf-8', errors='ignore') as f:
    for line in f:
        clas=line.partition(' ')[0]
        c[clas]+=1
f.close()
lf.write(str(len(c.keys())))
lf.write("\n")
for k in c:
    lf.write(k+" ")
    lf.write(str(c[k]))
    lf.write("\n")
for i in c:
    d = collections.Counter()
    with open(path, encoding='utf-8', errors='ignore') as f:
        for line in f:
            clas=line.partition(' ')[0]
            line=line.split(' ', 1)[1]
            if i == clas:
                words=re.findall(r'[^\s!,.?":;0-9]+', line)
                for j in words:
                    d[j]+=1
    #print(len(d.keys()))
    lf.write(i+" "+str(len(d.keys()))+"\n")
    n1 = 0
    for kk in d:
        n1+=d[kk]
        lf.write(kk+" ")
        lf.write(str(d[kk]))
        lf.write("\n")
    f.close()
lf.close()
